![](https://img.shields.io/f-droid/v/nightlock.peppercarrot.svg)

<a href='https://f-droid.org/en/packages/nightlock.peppercarrot/'>
<img alt='Get it on F-Droid' src='https://fdroid.gitlab.io/artwork/badge/get-it-on.png' height="75"/>
</a>
<a href='https://play.google.com/store/apps/details?id=nightlock.peppercarrot&pcampaignid=pcampaignidMKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1'>
<img alt='Get it on Google Play' src='https://play.google.com/intl/en_us/badges/static/images/badges/en_badge_web_generic.png' height="75"/>
</a>

# peppercarrot/e-reader

An E-Reader of [Pepper&Carrot](https://www.peppercarrot.com/) for Android, written in Kotlin.

# How to build

1. Clone this repository.
1. Import into Android Studio.
1. Download missing packages.
1. Sync project and build/run.

# How to contribute

1. Report bug or suggestions in the [issue tracker](https://framagit.org/peppercarrot/e-reader/issues).
1. Translate the app into another language, or improve existing translations.

# Special Thanks

* David Revoy: for making this awesome webcomic and endorsing this repository.
* Valvin: for providing me with the initial code for language detection.

# License
This project is licensed under [GNU GPL 3.0 or higher](LICENCE).
