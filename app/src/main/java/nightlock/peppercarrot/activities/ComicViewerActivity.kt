/*
 * Copyright (C) 2017 - 2021 Jihoon Kim <imsesaok@gmail.com, imsesaok@tuta.io>
 *
 * This file is part of Pepper&Carrot e-reader.
 *
 * Pepper&Carrot e-reader is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package nightlock.peppercarrot.activities

import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.MenuItem
import android.view.View
import android.view.WindowInsets
import android.view.WindowInsetsController
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.NavUtils
import nightlock.peppercarrot.R
import nightlock.peppercarrot.adapters.ComicViewerAdapter
import nightlock.peppercarrot.databinding.ActivityComicViewerBinding
import nightlock.peppercarrot.utils.ArchiveDataManager
import nightlock.peppercarrot.utils.Episode
import nightlock.peppercarrot.utils.Language

class ComicViewerActivity : AppCompatActivity() {
    private val hideHandler = Handler(Looper.getMainLooper())

    private var visibility: Boolean = false
    private val hideRunnable = Runnable { hide() }

    private val delayHideTouch = { bool:Boolean ->
        delayedHide(AUTO_HIDE_DELAY_MILLIS)
        bool
    }

    private var episodeId = 0
    private val language by lazy {
        intent.getParcelableExtra<Language>(Language.LANGUAGE)
    }
    private val episodeDb by lazy {
        ArchiveDataManager(this)
    }

    private lateinit var comicViewerAdapter: ComicViewerAdapter
    private lateinit var binding: ActivityComicViewerBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityComicViewerBinding.inflate(layoutInflater)
        setContentView(binding.root)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        visibility = true

        initListeners()
        registerVisibilityChangeListener()

        val episode = intent.getParcelableExtra<Episode>(Episode.EPISODE)
        episodeId = episode?.index ?: 0
        attachAdapter(episode, language)
    }

    private fun initListeners() = binding.apply {
        buttonBack.setOnClickListener {
            binding.comicViewpager.setCurrentItem(binding.comicViewpager.currentItem - 1, true)
            delayHideTouch(false)
        }
        buttonForward.setOnClickListener {
            binding.comicViewpager.setCurrentItem(binding.comicViewpager.currentItem + 1, true)
            delayHideTouch(false)
        }
        buttonForward.setOnLongClickListener {
            if (episodeId < episodeDb.length() - 1) {
                episodeId++
                attachAdapter(episodeDb.get(episodeId)!!, language!!)

                Toast
                        .makeText(it.context, R.string.jumped_to_next_episode, Toast.LENGTH_SHORT)
                        .show()
            }
            delayHideTouch(true)
        }
        buttonBack.setOnLongClickListener {
            if (episodeId > 1) {
                episodeId--
                attachAdapter(episodeDb.get(episodeId)!!, language!!)

                Toast
                        .makeText(it.context, R.string.jumped_to_previous_episode, Toast.LENGTH_SHORT)
                        .show()
            }
            delayHideTouch(true)
        }
    }

    @Suppress("DEPRECATION")
    private fun registerVisibilityChangeListener() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            window.decorView.setOnApplyWindowInsetsListener { _, windowInsets ->
                val statusOrNavigation = WindowInsets.Type.navigationBars() or WindowInsets.Type.statusBars()
                val isVisible = windowInsets.isVisible(statusOrNavigation)

                binding.fullscreenContentControls.visibility = if (isVisible) View.VISIBLE else View.GONE

                windowInsets
            }
        } else {
            window.decorView.setOnSystemUiVisibilityChangeListener { visibility ->
                val isVisible = visibility and View.SYSTEM_UI_FLAG_FULLSCREEN == 0

                binding.fullscreenContentControls.visibility = if (isVisible) View.VISIBLE else View.GONE
            }
        }
    }

    private fun attachAdapter(episode: Episode?, language: Language?) {
        comicViewerAdapter = ComicViewerAdapter(episode!!, language!!, supportFragmentManager)
        comicViewerAdapter.notifyDataSetChanged()
        binding.comicViewpager.adapter = comicViewerAdapter
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)
        delayedHide(100)
    }

    override fun onResume() {
        super.onResume()
        delayedHide(100)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        if (id == android.R.id.home) {
            NavUtils.navigateUpFromSameTask(this)
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    fun toggle() {
        if (visibility) hide()
        else {
            show()
            delayHideTouch(false)
        }
    }

    @Suppress("DEPRECATION")
    private fun hide() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            window.insetsController?.run {
                hide(WindowInsets.Type.navigationBars())
                hide(WindowInsets.Type.statusBars())
                hide(WindowInsets.Type.ime())
                systemBarsBehavior = WindowInsetsController.BEHAVIOR_SHOW_TRANSIENT_BARS_BY_SWIPE
            }
            window.setDecorFitsSystemWindows(false)
        } else {
            val visibilitySettings = (View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                    // Set the content to appear under the system bars so that the
                    // content doesn't resize when the system bars hide and show.
                    or View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    // Hide the nav bar and status bar
                    or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                    or View.SYSTEM_UI_FLAG_FULLSCREEN)

            window.decorView.systemUiVisibility = visibilitySettings
        }

        visibility = false
    }

    @Suppress("DEPRECATION")
    private fun show() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            window.insetsController?.run {
                show(WindowInsets.Type.navigationBars())
                show(WindowInsets.Type.statusBars())
                show(WindowInsets.Type.ime())
            }
            window.setDecorFitsSystemWindows(false)
        } else {
            val visibilitySettings =(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN)

            window.decorView.systemUiVisibility = visibilitySettings
        }

        visibility = true
    }

    private fun delayedHide(delayMillis: Int) {
        hideHandler.removeCallbacks(hideRunnable)
        hideHandler.postDelayed(hideRunnable, delayMillis.toLong())
    }

    companion object {
        private const val AUTO_HIDE_DELAY_MILLIS = 2500
    }
}
