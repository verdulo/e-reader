/*
 * Copyright (C) 2017 - 2021 Jihoon Kim <imsesaok@gmail.com, imsesaok@tuta.io>
 *
 * This file is part of Pepper&Carrot e-reader.
 *
 * Pepper&Carrot e-reader is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package nightlock.peppercarrot.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import nightlock.peppercarrot.TabListener
import nightlock.peppercarrot.adapters.ArchiveViewPagerAdapter

/**
 * Fragment holding a ViewPager consists of ArchiveFragments
 * Created by nightlock on 22/12/17.
 */

class ArchiveViewPagerFragment : ViewPagerFragment() {
    private lateinit var adapter: ArchiveViewPagerAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
            bundle: Bundle?): View {
        val view = super.onCreateView(inflater, container, bundle)
        adapter = ArchiveViewPagerAdapter(childFragmentManager, view.context)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.viewpager.adapter = adapter
    }

    override fun onStart() {
        super.onStart()

        if (adapter.count < 2){
            if (activity is TabListener) {
                (activity as TabListener).onDetached()
            }
        }
    }
}