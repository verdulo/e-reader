/*
 * Copyright (C) 2017 - 2021 Jihoon Kim <imsesaok@gmail.com, imsesaok@tuta.io>
 *
 * This file is part of Pepper&Carrot e-reader.
 *
 * Pepper&Carrot e-reader is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package nightlock.peppercarrot.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import nightlock.peppercarrot.TabListener
import nightlock.peppercarrot.databinding.FragmentViewpagerBinding

/**
 * Fragment holding a ViewPager; handles TabLayout attach/detachment
 * Created by nightlock on 18/03/18.
 */
abstract class ViewPagerFragment : Fragment() {
    private var _binding: FragmentViewpagerBinding? = null
    protected val binding get() = _binding!!

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
            bundle: Bundle?): View {
        _binding = FragmentViewpagerBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onStart() {
        super.onStart()

        if (activity is TabListener) {
            (activity as TabListener).onAttached().run {
                setupWithViewPager(binding.viewpager, true)
            }
        }
    }

    override fun onStop() {
        super.onStop()
        if (activity is TabListener) {
            (activity as TabListener).onDetached()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}