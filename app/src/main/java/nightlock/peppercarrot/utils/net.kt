/*
 * Copyright (C) 2017 - 2019 Jihoon Kim <imsesaok@gmail.com, imsesaok@tuta.io>
 *
 * This file is part of Pepper&Carrot e-reader.
 *
 * Pepper&Carrot e-reader is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package nightlock.peppercarrot.utils

import okhttp3.OkHttpClient
import okhttp3.Request
import org.json.JSONArray
import org.json.JSONObject
import org.jsoup.Jsoup
import java.io.IOException
import javax.net.ssl.SSLHandshakeException

/**
 * Utility functions for network tasks.
 * Created by Jihoon Kim on 2017-05-03.
 */

@Throws(SSLHandshakeException::class)
fun getEpisodeList(): List<Episode> {
    val episodeList = ArrayList<Episode>()
    val rawEpisodeList = getFromUrl("${ROOT_URL}episodes.json")
    val episodeJson = JSONArray(rawEpisodeList)

    for (i in 0 until episodeJson.length()) {
        val episode = episodeJson.getJSONObject(i)

        val name = episode.getString("name")
        val page = episode.getInt("total_pages")
        val languageJsonArray = episode.getJSONArray("translated_languages")

        val pncLanguageList = ArrayList<String>()
        for (j in 0 until languageJsonArray.length()) pncLanguageList += languageJsonArray.getString(
                j
        )

        episodeList += Episode(i, name, page, pncLanguageList)
    }
    return episodeList
}

@Throws(SSLHandshakeException::class)
fun getLanguageList(): List<Language> {
    val languageList = ArrayList<Language>()
    val langJson = JSONObject(getFromUrl("${ROOT_URL}langs.json"))

    langJson.keys().forEach { pncLanguage ->
        val langObject: JSONObject = langJson.getJSONObject(pncLanguage)
        val translatorsJsonArray = langObject.getJSONArray("translators")
        val name: String = langObject.getString("name")
        val localName: String = langObject.getString("local_name")
        val isoCode: String = langObject.getString("iso_code")
        val isoVersion: Int = langObject.getInt("iso_version")

        val translators = ArrayList<String>()
        (0 until translatorsJsonArray.length()).forEach { j ->
            translators += translatorsJsonArray.getString(j)
        }

        languageList += Language(name, localName, translators, pncLanguage, isoCode, isoVersion)
    }
    return languageList
}

fun getEasterEggs(): List<String> = Jsoup
        .parse(getFromUrl("${ROOT_URL}0ther/artworks/low-res/"))
        .body()
        .getElementsByAttributeValueMatching("href", "^[^?\\/].+\\.\\w{3,4}$")
        .map { element -> "${ROOT_URL}0ther/artworks/low-res/${element.attr("href")}" }


fun getFromUrl(url: String): String? = OkHttpClient()
        .newCall(Request.Builder().header("User-Agent", Constants.UA).url(url).build())
        .execute()
        .use {
            return it.body?.string()
        }

fun pokeAt(url: String): Boolean = try {
    OkHttpClient()
            .newCall(Request.Builder().header("User-Agent", Constants.UA).url(url).build())
            .execute()
            .use {
                it.isSuccessful
            }
} catch (e: IOException) {
    false
}