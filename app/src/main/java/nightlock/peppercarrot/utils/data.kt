/*
 * Copyright (C) 2017 - 2021 Jihoon Kim <imsesaok@gmail.com, imsesaok@tuta.io>
 *
 * This file is part of Pepper&Carrot e-reader.
 *
 * Pepper&Carrot e-reader is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package nightlock.peppercarrot.utils

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

/**
 * Defines data classes
 * Created by Jihoon Kim on 08/07/17.
 */

@Parcelize
data class Episode(val index: Int, val name: String, val pages: Int,
        val supported_languages: List<String>) : Parcelable {

    companion object {
        const val EPISODE = "ep"
    }
}

@Parcelize
data class Language(val name: String, val localName: String, val translators: List<String>,
        val pncCode: String, val isoCode: String, val isoVersion: Int) : Parcelable {

    companion object {
        const val LANGUAGE = "lang"
        const val SELECTED_LANGUAGES = "selected_langs"
    }
}
