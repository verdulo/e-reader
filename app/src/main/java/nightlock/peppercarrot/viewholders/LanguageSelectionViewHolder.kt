/*
 * Copyright (C) 2017 - 2019 Jihoon Kim <imsesaok@gmail.com, imsesaok@tuta.io>
 *
 * This file is part of Pepper&Carrot e-reader.
 *
 * Pepper&Carrot e-reader is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package nightlock.peppercarrot.viewholders

import android.content.SharedPreferences
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.preference.PreferenceManager
import nightlock.peppercarrot.R
import nightlock.peppercarrot.utils.Language
import nightlock.peppercarrot.utils.getSafePrefString
import nightlock.peppercarrot.utils.jsonToList
import nightlock.peppercarrot.utils.listToJson

/**
 * ViewHolder for Individual Language Selection.
 * Created by nightlock on 21/12/17.
 */
class LanguageSelectionViewHolder private constructor(v: View) :
        BaseViewHolder<Pair<Language, Boolean>>(v) {
    private val checkBox: CheckBox = v.findViewById(R.id.language_checkbox)
    private val cardView: CardView = v.findViewById(R.id.language_selection_card)
    private val languageName: TextView = v.findViewById(R.id.language_name_text)
    private val recommendedText: TextView = v.findViewById(R.id.recommended_language_text)
    private val preference: SharedPreferences =
            PreferenceManager.getDefaultSharedPreferences(v.context
            )
    private lateinit var language: Language

    init {
        cardView.setOnClickListener { checkBox.performClick() }
        checkBox.setOnClickListener {
            val languages: MutableList<String>
            val selectedLangsJson = getSafePrefString(preference, Language.SELECTED_LANGUAGES, "[]")
            if (selectedLangsJson != "[]") {
                languages = jsonToList(selectedLangsJson)!!

                if (!checkBox.isChecked) languages.remove(language.pncCode)
                else languages.add(language.pncCode)
            } else {
                languages = ArrayList()
                languages.add(language.pncCode)
            }

            preference.edit().putString(Language.SELECTED_LANGUAGES, listToJson(languages)).apply()
        }
    }

    override fun onBind(data: Pair<Language, Boolean>) {
        this.language = data.first
        val recommended = data.second
        val visibility = if (recommended) View.VISIBLE else View.GONE

        recommendedText.visibility = visibility
        languageName.text = language.localName

        val selectedLangsJson = getSafePrefString(preference, Language.SELECTED_LANGUAGES, "")
        checkBox.isChecked = selectedLangsJson.contains(language.pncCode)
    }

    companion object {
        fun newInstance(viewGroup: ViewGroup): LanguageSelectionViewHolder {
            val view =
                    LayoutInflater
                            .from(viewGroup.context)
                            .inflate(R.layout.fragment_languageselection, viewGroup, false)
            return LanguageSelectionViewHolder(view)
        }
    }
}
